const path = require('path')
const BrotliPlugin = require('brotli-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

module.exports = {
  lintOnSave: true,
  pwa: {
    workboxOptions: {
      skipWaiting: true
    }
  },
  chainWebpack: (config) => {
    config.module.rule('eslint').use('eslint-loader').options({ fix: true })
  },
  transpileDependencies: ['vuetify'],
  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false,
      includeLocales: false,
      enableBridge: true
    }
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve('src'),
        assets: path.resolve('src/assets'),
        components: path.resolve('src/components'),
        layouts: path.resolve('src/components/layouts')
      }
    },
    plugins: [
      ...(process.env.NODE_ENV === 'production'
        ? [
            new BrotliPlugin({
              asset: '[path].br[query]',
              test: /\.(js|css|html|svg)$/,
              threshold: 10240,
              minRatio: 0.8
            })
          ]
        : [])
    ],
    optimization: {
      minimize: true,
      minimizer: [
        new UglifyJsPlugin({
          include: /\.min\.js$/
        })
      ],
      splitChunks: {
        chunks: 'all'
      }
    }
  },
  css: {
    loaderOptions: {
      scss: {
        additionalData: '@import "~@/styles/scss/variables";'
      }
    }
  }
}

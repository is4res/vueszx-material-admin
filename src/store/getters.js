const isLoading = (state) => state.system.isLoading
const isLogin = (state) => state.user.isLogin
const permissions = (state) => state.user.permissions

export default { isLoading, isLogin, permissions }

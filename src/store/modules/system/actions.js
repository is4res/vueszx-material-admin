export default {
  showLoading(context) {
    context.commit('setLoading', true)
  },
  hideLoading(context) {
    context.commit('setLoading', false)
  }
}

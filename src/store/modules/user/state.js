export default {
  isLogin: false,
  permissions: [
    'about.view_about',
    'components.view_widget',
    'components.add_widget',
    'components.delete_widget',
    'components.view_table_normal',
    'components.add_table_normal',
    'components.change_table_normal',
    'components.delete_table_normal',
    'components.add_table_server_side',
    'components.change_table_server_side',
    'components.delete_table_server_side'
  ]
}

export default {
  login(context, $api, data) {
    return new Promise((resolve) => {
      context.dispatch('showLoading')
      setTimeout(async () => {
        await context.dispatch('updateLoginInfo')
        resolve(data)
        context.dispatch('hideLoading')
      }, 5000)
    })
  },
  forgetPassword(context, $api, data) {
    return new Promise((resolve) => {
      context.dispatch('showLoading')
      setTimeout(async () => {
        resolve(data)
        context.dispatch('hideLoading')
      }, 5000)
    })
  },
  logout(context) {
    context.commit('setLogin', false)
  },
  verifyToken(context, token) {
    return new Promise((resolve) => {
      setTimeout(async () => {
        await context.dispatch('updateLoginInfo')
        resolve(token)
      }, 5000)
    })
  },
  updateLoginInfo(context) {
    return Promise.all([context.commit('setLogin', true)])
  },
  permissions(context, value) {
    context.commit('setPermissions', value)
  }
}

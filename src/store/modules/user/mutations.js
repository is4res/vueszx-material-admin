export default {
  setLogin(state, value) {
    state.isLogin = value
  },
  setPermissions(state, value) {
    state.permissions = value
  }
}

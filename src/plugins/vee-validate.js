import { extend } from 'vee-validate'
import { required, email, min_value, max_value } from 'vee-validate/dist/rules'

const rules = {
  required,
  email,
  min_value,
  max_value
}
Object.keys(rules).map((key) => extend(key, rules[key]))

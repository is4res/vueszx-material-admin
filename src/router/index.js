import Vue from 'vue'
import VueRouter from 'vue-router'
import CmsLayout from 'layouts/cms/CmsLayout'
import AuthLayout from 'layouts/auth/AuthLayout'
import Empty from 'layouts/Empty.vue'
import { beforeRouter, afterRouter } from './middleware'

Vue.use(VueRouter)

const lazyImport = (path) => () => import(`@/views/${path}`)

const routes = [
  {
    path: '/',
    component: CmsLayout,
    meta: {
      isDrawer: true
    },
    children: [
      {
        name: 'home',
        path: '',
        component: lazyImport('Home'),
        meta: {
          icon: 'mdi-home',
          isHideBreadcrumb: true
        }
      },
      {
        name: 'about',
        path: 'about',
        component: lazyImport('About'),
        meta: {
          icon: 'mdi-alert-circle',
          permission: 'about.view_about'
        }
      },
      {
        name: 'components',
        path: 'components',
        component: Empty,
        meta: {
          icon: 'mdi-widgets'
        },
        children: [
          {
            name: 'table',
            path: 'table',
            component: Empty,
            children: [
              {
                name: 'tableNormal',
                path: 'normal',
                component: lazyImport('components/table/TableNormal'),
                meta: {
                  permission: 'components.view_table_normal'
                }
              },
              {
                name: 'tableServerSide',
                path: 'server-side',
                component: lazyImport('components/table/TableServerSide')
              }
            ]
          },
          {
            name: 'widget',
            path: 'widget',
            component: lazyImport('components/Widget'),
            meta: {
              icon: 'mdi-card-multiple',
              permission: 'components.view_widget'
            }
          },
          {
            name: 'forms',
            path: 'forms',
            component: lazyImport('components/Forms')
          }
        ]
      }
    ]
  },
  {
    path: '/auth',
    component: AuthLayout,
    children: [
      {
        name: 'login',
        path: 'login',
        component: lazyImport('auth/Login')
      },
      {
        name: 'forget-password',
        path: 'forget-password',
        component: lazyImport('auth/ForgetPassword')
      }
    ]
  },
  {
    path: '/logout',
    redirect: { name: 'login' }
  },
  {
    path: '*',
    redirect: { name: 'home' }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// before router change
router.beforeEach(beforeRouter)

// after router change
router.afterEach(afterRouter)

export default router

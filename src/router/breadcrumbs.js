export default {
  root: {
    name: 'home',
    text: 'menu.home'
  },
  routes: [
    {
      name: 'home',
      text: 'menu.home'
    },
    {
      name: 'about',
      text: 'menu.about'
    },
    {
      name: 'components',
      text: 'menu.components',
      disabled: true,
      children: [
        {
          name: 'table',
          text: 'menu.table',
          disabled: true,
          children: [
            {
              name: 'tableNormal',
              text: 'menu.tableNormal'
            },
            {
              name: 'tableServerSide',
              text: 'menu.tableServerSide'
            }
          ]
        },
        {
          name: 'widget',
          text: 'menu.widget'
        },
        {
          name: 'forms',
          text: 'menu.forms'
        }
      ]
    }
  ]
}

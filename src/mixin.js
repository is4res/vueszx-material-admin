import Vue from 'vue'
import request from '@/utils/request'
import Swal from 'sweetalert2/dist/sweetalert2.js'

const swalBtnClass = 'v-btn v-btn--has-bg theme--light v-size--default mx-4'
const swal = Swal.mixin({
  target: '#app',
  customClass: {
    confirmButton: `${swalBtnClass} primary`,
    cancelButton: `${swalBtnClass} error`
  },
  buttonsStyling: false
})

Vue.mixin({
  computed: {
    staticAlert() {
      // default alert modal pack
      return {
        success: {
          icon: 'success',
          title: this.$t('alert.success.title'),
          text: this.$t('alert.success.text')
        },
        error: {
          icon: 'error',
          title: this.$t('alert.error.title'),
          text: this.$t('alert.error.text')
        }
      }
    }
  },
  methods: {
    /**
     * request to api with axios
     * @param {Object} options
     *  - disableAlert: to disable alert on response
     *  - disableLoading: to disable loading screen on request
     */
    $api(options) {
      options.$alert = this.$alert
      return request(options)
    },
    /**
     *
     * @param {String} status
     *  - success, error, custom
     * @param {Object} options will use on status is custom
     *  - type
     *  - title
     *  - text
     */
    $alert(status, options = {}) {
      const alert = {
        ...this.staticAlert,
        custom: options
      }

      if (alert[status]) {
        return swal.fire(alert[status])
      }
    },
    /**
     * confirm delete modal
     * @param {Array} data
     */
    $confirmDelete(data, deleteFunc) {
      if (!data.length) {
        // minimum select at least one
        return this.$alert('custom', {
          icon: 'warning',
          title: this.$t('alert.submitDelete.warning.title'),
          text: this.$t('alert.submitDelete.warning.text')
        })
      }

      this.$alert('custom', {
        icon: 'warning',
        title: this.$t('alert.submitDelete.title'),
        text: this.$t('alert.submitDelete.text'),
        showCancelButton: true,
        confirmButtonText: this.$t('alert.submitDelete.confirmButtonText')
      }).then((result) => {
        if (result.value) {
          // call $onDelete to do what it should
          deleteFunc(data).then(() => {
            this.$alert('custom', {
              icon: 'success',
              title: this.$t('alert.submitDelete.done.title'),
              text: this.$t('alert.submitDelete.done.text')
            })
          })
        }
      })
    }
  }
})
